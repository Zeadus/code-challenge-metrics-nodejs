var sql = require('../../db/sql').user;
var db = require('../../db').db;
var login = function login (values,fn) {
  let response = {};
  db.one(sql.login, values)
    .then(function(data) {
      console.log(data);
      fn(null, data);
    })
    .catch(function(error) {
      response = {
        statusCode: 500,
        message: "Database Error",
        error: error.message
      };
      console.log(error);
      fn(response);
    })
};

module.exports = login;