var sql = require('../../db/sql').data;
var db = require('../../db').db;
var getData = function getData (values,fn) {
  db.any(sql.getData, values)
    .then(function(data) {
        fn(null, data);
    })
    .catch(function(error) {
      var response = {
        statusCode: 500,
        message: "Database Error",
        error: error.message
      };
      console.log(error);
      fn(response);
    })
};

module.exports = getData;