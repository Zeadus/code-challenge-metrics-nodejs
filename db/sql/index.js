'use strict';

var QueryFile = require('pg-promise').QueryFile;
var path = require('path');

// Helper for linking to external query files;
function sql(file) {
  var qPath = path.join(__dirname, '/../sql/', file);

  var options = {
    minify: true,
    params: {
      schema: 'public' // 'public' is the default schema
    }
  };

  return new QueryFile(qPath, options);
}

module.exports = {
  data: {
    getData: sql('data/get-data.sql'),
  },

  user: {
      login: sql('user/login.sql')
  }
}