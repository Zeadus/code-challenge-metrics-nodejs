'use strict';

var config = require('../config/config');
var promise = require('bluebird');

// pg-promise initialization options:
var options = {
  promiseLib: promise
};

// Load and initialize pg-promise:
var pgp = require('pg-promise')(options);

// Create the database instance:
var db = pgp(config.database);



module.exports = {
  pgp,
  db
};
