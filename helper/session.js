'use strict';

/**
 * Module dependencies.
 * @private
 */
var config = require('../config/config');
var redis = require("redis");
var Session = require('express-session');
var RedisStore = require('connect-redis')(Session);
var redisClient = redis.createClient({
  host: config.redisHost,
  port: config.redisPort
});

/**
 * Expose the middleware.
 */
module.exports = session;

function session() {
  var sessionSecret = config.sessionSecret || 'secret';
  var sessionSaveUninitialized = config.sessionSaveUninitialized || false;
  var sessionResave = config.sessionResave || false;
  var redisHost = config.redisHost || 'localhost';
  var redisPort = config.redisPort || 6379;
  var redisTtl = config.redisTtl || 86400;

  var sessionMiddleware = Session({
    cookie: {
      maxAge: 2 * 24 * 60 * 60 * 1000,
      // httpOnly: true,  secure: true  
    },
    secret: sessionSecret,
    store: new RedisStore({host: redisHost, port: redisPort, client: redisClient, ttl: redisTtl}),
    saveUninitialized: sessionSaveUninitialized,
    resave: sessionResave
  });

  return function session(req, res, next) {
    var tries = 3;
    function lookupSession(error) {
      if (error) {
        return next(error);
      }
      tries -= 1;
      if (req.session !== undefined) {
        return next();
      }
      if (tries < 0) {
        var salida = {
          statusCode: 500,
          status: "error",
          message: "Session error",
          errors: []
        };
        res.status(salida.statusCode);
        return res.json(salida);
      }
      sessionMiddleware(req, res, lookupSession);
    }
    lookupSession();
  };
}
