var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var router = require('./controller');
var expressSession = require('express-session');
var RedisStore = require('connect-redis');
var redis = require('redis');
var session = require('./helper/session');


const app = express();
app.use(session({}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

const port = process.env.PORT || 3000;
app.use('/', router);
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to this API.',
}));

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});

