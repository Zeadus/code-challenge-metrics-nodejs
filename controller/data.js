var data = require('../model/data');

module.exports = function(req,res) {
	var response = {};
	console.log(req.query);
	data.getData({
		limit: isNaN(parseInt(req.query.limit, 10)) ? 'ANY' : req.query.limit,
		sort: req.query.sort === 'asc' || req.query.sort === 'desc' ? req.query.sort : '',
		sortField: req.query.sortField ? req.query.sortField : '',
	}, function(err, data) {
		if(data) {
            if (req.query.fields && req.query.fields.length > 0) {
                data = data.map(m => {
                    const d = {}
                    req.query.fields.forEach(field => {
                        d[field] = m[field];
                    });
                    return d;
                });
            }

			response = {
				response: data,
			};
			return res.status(200).json(response).end();
		}
		else {
			console.log(err);
			return res.status(err.statusCode).json(err).end();
		}
	});
};