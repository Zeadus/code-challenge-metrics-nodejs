var express = require('express');
var router = express.Router();


router.post('/login', require('./login'));
router.get('/data', require('../middleware/session'), require('./data'));
router.get('/files/list', require('../middleware/session'), require('./fileList'));
router.get('/files/metrics', require('../middleware/session'), require('./metrics'));

module.exports = router;
