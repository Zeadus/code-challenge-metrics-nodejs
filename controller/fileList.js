var data = require('../model/data');
var fs = require('fs');

module.exports = function(req,res) {
    var filesInfo = [];
    fs.readdir('./data', (err, files) => {
        files.forEach((file, i) => {
          console.log(file);
          console.log(i);
          fs.stat('./data/' + file, (err, stats) => {
            filesInfo.push({
                file: file, 
                size: req.query.humanreadable === 'true' ? formatBytes(stats.size, 0) : stats.size
            });
            if (i === files.length -1) {
                return res.status(200).json({response: filesInfo}).end();
            }
          });
        });
    });   
};

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}