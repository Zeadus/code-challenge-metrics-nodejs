var data = require('../model/data');
var child = require('child_process');
var config = require('../config/config');
var redis = require('redis');
var client = redis.createClient(config.redisPort, config.redisHost);
module.exports = function(req,res) {
    if (req.query.delete === 'true') {
        client.del(req.query.filename, function(result) {
            return res.status(200).json({message: 'deleted'});
        })
    } else {
        client.get(req.query.filename, function(err, result) {
            if (result) {
                return res.status(200).json({metrics: JSON.parse(result)});
            } else {
                var process = child.spawn('node', ['controller/process-file.js', req.query.filename], {detached: false, shell: true});
                process.stdout.on('data', function (data) {
            
                    console.log('stdout: ' + data);
            
                
                });
                
                 
                
                process.stderr.on('data', function (data) {
                
                    console.log('stderr: ' + data);
                
                });
                
                 
                
                process.on('close', function (code) {
                
                    console.log('Child process exit with code: ' + code);
                
                });
    
                return res.status(200).json({status: 'Started', started: new Date().toUTCString()});
            }
        })
    }
};