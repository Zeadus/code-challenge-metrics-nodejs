var User = require('../model/user');
var jwt = require('jsonwebtoken');
var config = require('../config/config');
module.exports = function(req,res) {
	let response = {};
	if(!req.body.user || !req.body.password) {
		response = {
			statusCode: 400,
			status: 'Error',
			message: 'Required mandatory field not found'
		}
		return res.status(response.statusCode).json(response).end();
    }
    console.log(req.body);
	User.login({
		user: req.body.user
    }, function(err, data) {
        if (!data) {
            response = {
                statusCode: 404,
                status: 'Not Found',
                message: 'User Not Found'
            }
            return res.status(response.statusCode).json(response).end();
        }
        // En caso de utilizar contraseñas encriptadas, para este caso no las encripte e hice una comparacion directa
        // if(!bcrypt.compareSync(req.body.password, data.password)) {
        //     response = {
        //         statusCode: 404,
        //         status: 'error',
        //         message: 'Wrong Password'
        //     };
        //     return res.status(response.statusCode).json(response).end();
        // }

        if (req.body.password !== data.password) {
            response = {
                statusCode: 400,
                status: 'error',
                message: 'Wrong Password'
            };
            return res.status(response.statusCode).json(response).end();
        }

        if (req.session && req.session.token) {
            response = {
                statusCode: 200,
                status: 'Success',
                message: 'Already logged in',
                token: req.session.token,
            }
            return res.status(response.statusCode).json(response).end();
        } else {
            const token = jwt.sign(
                {
                    user: data.user		
                },
                config.jwtSecret,
                {
                    algorithm: 'HS256',
                    expiresIn: '1 days',
                    issuer: 'DS',
                    audience: data.user.toString(),
                    jwtid: data.user.toString()
                }
            );

            try {
                req.session.user = data.userid;
                req.session.token = token;
            } catch (ex) {
                response = {
                    statusCode: 500,
                    status: 'error',
                    message: 'Unknown Error'
                };
            }
            
            if (response.statusCode === 500) {
                return res.status(response.statusCode).json(response).end();
            }

            response = {
				statusCode: 200,
				status: 'success',
				message: 'Logged in',
				token: token,
			};
			res.setHeader("auth-token", response.token);
			return res.status(response.statusCode).json(response).end();
        }
    });
}