var csv = require('fast-csv');
var config = require('../config/config');
var redis = require('redis');
var client = redis.createClient(config.redisPort, config.redisHost);
var count = 0;
var metrics = [];
var segmentIndex = {};
var uniquesIndex = [];
var rowSegments = [];
var metric = {};

var data = {
    status: 'Processing',
    started: new Date().toUTCString(),
    finished: '',
    metrics
};

console.log();

client.set(process.argv[2], JSON.stringify(data), (result) => {
    
});

let stream = csv.parseFile(`data/${process.argv[2]}`, {delimiter: '\t'})
.on("error", error => {
    console.log(error);
})
.on("data", function(row) {
    rowSegments = row[1].split(',');
    rowSegments.forEach(element => {
        if (segmentIndex[element]) {
            if (segmentIndex[element][row[2]]) {
                metrics[segmentIndex[element].index].uniques[segmentIndex[element][row[2]]].count += 1;
            } else {
                metrics[segmentIndex[element].index].uniques.push({
                    "country": row[2],
                    "count": 1
                });
                segmentIndex[element][row[2]] = metrics[segmentIndex[element].index].uniques.length - 1;
            }
        } else {
            metric = {
                "segmentId": element,
                "uniques": [{
                    "country": row[2],
                    "count": 1
                }]
            };
            metrics.push(metric);
            segmentIndex[element] = {
                "index": metrics.length - 1,
            };
            segmentIndex[element][row[2]] = 0;
        }
    });
})
.on("end", count => {
    console.log(`finished streaming ${count} lines`)
    data.metrics = metrics;
    data.finished = new Date().toUTCString();
    data.status = 'Finished';
    client.set(process.argv[2], JSON.stringify(data), (result) => {
        
    });
})