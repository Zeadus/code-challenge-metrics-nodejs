CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE user(
    user UUID default uuid_generate_v1mc() PRIMARY KEY,
    username VARCHAR(30),
    password VARCHAR
);

CREATE TABLE data(
    data UUID default uuid_generate_v1mc() PRIMARY KEY,
    name VARCHAR(100),
    segment1 BOOLEAN,
    segment2 BOOLEAN,
    segment3 BOOLEAN,
    segment4 BOOLEAN,
    platformId integer,
    clientId integer
);

INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Ricardo', true, false, false, true, 1, 2);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Romina', true, true, true, true, 2, 3);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Joaquin', false, false, false, true, 4, 4);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Lucia', false, false, true, true, 10, 1);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Alejandro', true, false, false, true, 1, 3);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Alicia', true, true, true, true, 5, 3);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Joaquin', false, false, true, false, 5, 4);
INSERT INTO data(name, segment1, segment2, segment3, segment4, platformId, clientid)
VALUES('Lucia', true, true, true, true, 10, 1);


INSERT INTO "user"(username, password) VALUES('login', 'retarget');