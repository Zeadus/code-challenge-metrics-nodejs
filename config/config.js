module.exports = {
    database: 'postgres://postgres:password@localhost:5432/retargetly',
    port: 3000,
    host: 'http://127.0.0.1:8002',
    sessionSecret: 'b3aa66vhc7d09b774f2b30bby07e67fe0ad140c',
    redisHost: 'localhost',
    redisPort: 6379,
    redisTtl: 86400,
    jwtSecret: 'uee89abv1c0f16e6247l3di70dv9a7r6e28b10a3'
};
  