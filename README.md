Retargetly code challenge:

Pasos para arrancar el API:

-Crear una DB de PGSQL llamada Retargetly, utilizar el query del archivo db.sql en la raiz para crear las tablas necesarias y los datos.

-Insertar los archivos .tsv en la carpeta /data

-Correr "npm install"

-En la raiz existe una coleccion de postman (Retargetly.postman_collection.json) para probar los EPs


Para desarrollar lo solicitado se utilizo:

-PostgreSQL para crear el modelo de datos y guardar los datos en el paso 1, PG-Promise para realizar la conexion a la DB y los queries.

-Redis para guardar la informacion de la sesion (token) y para guardar los metrics de los archivos.

-fast-csv para hacer un stream de la informacion de los archivos y no quedarse sin memoria


Notas:

-En este caso no utilice encriptamiento de contraseñas para no cargar tanto el login. El archivo login.js del controlador tiene el codigo comentado para comparar una contraseña encriptada

-El EP /files/metrics tambien recibe un parametro "delete=true" que permite borrar las metricas que se guardaron del archivo para probar de nuevo. 