'use strict';
var jwt = require('jsonwebtoken');
var config = require('../config/config')
module.exports = function(req, res, next) {
  var salida = {};
  var token = req.headers.authorization;
  if(!req.session.token) {
    salida = {
      statusCode: 403,
      status: 'error',
      message: 'No session created'
    };
    return res.status(salida.statusCode).json(salida).end();
  }
  if (!token) {
    salida = {
      statusCode: 403,
      status: "error",
      message: "No token provided",
      errors: []
    };
    res.status(salida.statusCode);
    res.json(salida);
    return false;
  }
  if (token) {
    jwt.verify(token, config.jwtSecret, function(err, decoded) {
      if (err) {
        salida = {
          statusCode: 403,
          status: "error",
          message: "Invalid Token",
          errors: []
        };
        res.status(salida.statusCode);
        res.json(salida);
        console.log("token error");
        return false;
      }
      if (req.session.token !== token) {
          salida = {
            statusCode: 403,
            status: "error",
            message: "Invalid Token",
            errors: []
          };
          res.status(salida.statusCode);
          res.json(salida);
          console.log("Session error");
          return false;
      }
      next();
    });
  }
};
